package com.example.quizapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.example.quizapp.databinding.ActivityMainBinding
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var rightAnswer:String? = null
    private var rightAnswerCount = 0
    private var quizCount = 1
    private val QUIZ_COUNT = 7

    private val quizData = mutableListOf(
        mutableListOf("大橋和也の生年月日","1997/08/09","1996/08/07","1997/08/04","1998/09/07"), // 大橋和也HB
        mutableListOf("大西流星の生年月日","2001/08/07","2001/09/07","2000/08/08","2000/08/07"), // 大西流星HB
        mutableListOf("藤原丈一郎の生年月日","1996/02/08","1995/02/08","1996/02/07","1995/02/07"), // 藤原丈一郎HB
        mutableListOf("西畑大吾の生年月日","1997/01/09","1997/01/10","1996/01/10","1996/01/09"), // 西畑大吾HB
        mutableListOf("道枝駿佑の生年月日","2002/07/25","2001/07/25","2002/07/24","2001/07/24"), // 道枝駿佑HB
        mutableListOf("長尾謙杜の生年月日","2002/08/15","2001/08/15","2001/08/15","2002/08/14"), // 長尾謙杜HB
        mutableListOf("高橋恭平の生年月日","2000/02/28","1998/02/29","2000/02/29","2000/02/27")  // 高橋恭平HB

    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        quizData.shuffle()

        showNextQuiz()
    }

    // クイズを出題する
    fun showNextQuiz(){
        // カウントラベルの更新
        binding.countLabel.text = getString(R.string.count_label,quizCount)

        // クイズを1問取り出す
        val quiz = quizData[0]

        // 問題セット
        binding.questionLabel.text = quiz[0]

        // 正解をセット
        rightAnswer = quiz[1]

        //　問題を削除
        quiz.removeAt(0)

        // 正解と選択肢をシャッフル
        quiz.shuffle()

        // 選択肢をセット
        binding.answerBtn1.text = quiz[0]
        binding.answerBtn2.text = quiz[1]
        binding.answerBtn3.text = quiz[2]
        binding.answerBtn4.text = quiz[3]

        // 出題したクイズを削除する
        quizData.removeAt(0)
    }

    // 解答ボタンが押されたら呼ばれる
    fun checkAnswer(view: View){
        // どのボタンが押されたか
        val answerBtn : Button = findViewById(view.id)
        val btnText = answerBtn.text.toString()

        // ダイアログのタイトルの作成
        val alertTile : String
        if ( btnText == rightAnswer ){
            alertTile = "正解!"
            rightAnswerCount++
        }else{
            alertTile = "不正解..."
        }

        // ダイアログの作成
        AlertDialog.Builder(this)
            .setTitle(alertTile)
            .setMessage("答え:$rightAnswer")
            .setPositiveButton("OK"){
                dialogInterface, i -> cheakQuizCount()
            }
            .setCancelable(false)
            .show()
    }

    // 問題数をちチェックする
    fun cheakQuizCount(){
        if( quizCount == QUIZ_COUNT ){
            // 結果画面を表示
            val intent = Intent(this@MainActivity,ResultActivity::class.java)
            intent.putExtra("RIGHT_ANSWER_COUNT",rightAnswerCount)
            startActivity(intent)

        }else {
            quizCount++
            showNextQuiz()
        }
    }
}